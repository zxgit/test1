package com.kuang;

import com.alibaba.fastjson.JSON;
import com.sun.media.sound.UlawCodec;
import lombok.SneakyThrows;
import org.apache.http.client.HttpClient;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;

import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import com.kuang.pojo.uesr;

import javax.xml.transform.Source;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

@SpringBootTest
class KuangshenEsApiApplicationTests {

    @Autowired
    @Qualifier("restHighLevelClient")
    RestHighLevelClient client;


    @SneakyThrows
    @Test
    void contextLoads() {

        // 创建索引请求
        CreateIndexRequest request = new CreateIndexRequest("kuang_index");
        // 执行请求
        CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);

        System.out.println(createIndexResponse);
    }

    @SneakyThrows
    @Test
    void test() {

        uesr uesr = new uesr("ssss", "ssdf");

        // 创建请求
        IndexRequest request = new IndexRequest("kuang_index");
        // 创建规则
        request.id("1");
        request.timeout(TimeValue.timeValueSeconds(1));
        // 创建数据请求
        request.source(JSON.toJSONString(uesr), XContentType.JSON);
        // 客户端发送请求
		IndexResponse indexResponse = client.index(request, RequestOptions.DEFAULT);
		// 获取响应结果
		System.out.println(indexResponse.toString());
		System.out.println(indexResponse.status());

        // 获取文档
        GetRequest getRequest = new GetRequest("kuang_index", "1");
        GetResponse response = client.get(getRequest, RequestOptions.DEFAULT);
        String sourceAsString = response.getSourceAsString();
        System.out.println(sourceAsString);

        // 判断是否存在
        boolean exists = client.exists(getRequest, RequestOptions.DEFAULT);
        System.out.println(exists);

        // 更新文档信息
        UpdateRequest updateRequest = new UpdateRequest("kuang_index", "1");
        updateRequest.timeout("1s");
        // 修改对象
        uesr zhangsan = new uesr("zhangsan", "17");
        // 修改doc
        updateRequest.doc(JSON.toJSONString(zhangsan), XContentType.JSON);
        // 执行api
        UpdateResponse update = client.update(updateRequest, RequestOptions.DEFAULT);
        System.out.println(update.status());
        // 获取api
        GetResponse response1 = client.get(getRequest, RequestOptions.DEFAULT);
        System.out.println(response1.getSourceAsString());

        // 删除索引
        DeleteRequest deleteRequest = new DeleteRequest("test3", "2");
        deleteRequest.timeout("1s");
        DeleteResponse deleteResponse = client.delete(deleteRequest, RequestOptions.DEFAULT);
        System.out.println(deleteResponse.status());


    }


    @SneakyThrows
    @Test
    void testlist() {
        BulkRequest bulkRequest = new BulkRequest();

        bulkRequest.timeout("10s");

        ArrayList<uesr> userList = new ArrayList<>();
        userList.add(new uesr("zhangsan1", "17"));
        userList.add(new uesr("zhangsan2", "17"));
        userList.add(new uesr("zhangsan3", "17"));
        userList.add(new uesr("zhangsan4", "17"));
        userList.add(new uesr("zhangsan5", "17"));
        userList.add(new uesr("zhangsan6", "17"));
        userList.add(new uesr("zhangsan7", "17"));

        for (int i = 0; i < userList.size(); i++) {
            // 批量新增
            bulkRequest.add(
                    new IndexRequest("kuang_index")
                            .id("" + (i + 1))
                            .source(JSON.toJSONString(userList.get(i)), XContentType.JSON));
        }
        BulkResponse bulkResponse = client.bulk(bulkRequest, RequestOptions.DEFAULT);
        System.out.println(bulkResponse.hasFailures());


    }

    @SneakyThrows
    @Test
    void testsearch(){
        // 搜索请求
        SearchRequest request = new SearchRequest("kuang_index");
        // 搜索条件构造
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(1);
        searchSourceBuilder.size(10);

        searchSourceBuilder.query(QueryBuilders.termQuery("age", "17"));
        request.source(searchSourceBuilder);

        SearchResponse search = client.search(request, RequestOptions.DEFAULT);
        for (SearchHit hit : search.getHits().getHits()) {

            Map<String, Object> sourceAsMap = hit.getSourceAsMap();
            System.out.println(sourceAsMap);
        }


    }



}
